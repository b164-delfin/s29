const express = require('express');

//Created an application using express
// app is our server 
const app = express();

const port = 4000;

//Middleware are software that provide services 

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//mock database
let users = []


//Routes/endpoint
//http://localhost:4000
//get
app.get("/", (req, res) => {
	res.send("Hello World")
});

//post

app.post("/hello", (req, res) => {
	//req.body contains the contents/data of the request body

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

//Create a POST route to register a user.

app.post("/signup", (req, res) => {
	let body = req.body;
	console.log(body);

	//validation
	//If contents of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
	} else {
		res.send("Please input BOTH username and password")
	}
})


//This routes expects to receive a PUT request at the URI "/change-password"
//put
//This will update the password or a user that matches the information provided in the client

app.put("/change-password", (req, res) => {

let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated`;


			break;

		}else {
			message = "User does not exist"
		}
	}

	res.send(message);

})

app.get("/home", (req, res) => {
	res.send("Welcome to the Home Page!")
});


app.get('/users', (req, res) => res.send(users));


app.delete('/delete-user', (req, res) => {

	let message;
  if(users.length != 0) {

  	for (let i = 0; i < users.length; i++) {
  		if(req.body.username == users[i].username) {
  			users.splice(users[i], 1);

  			message = `User ${req.body.username} has been deleted`;

  			break;
  		}
  	}
  } else {
  	message = "database had zero record"
  }

res.send(message);

})


app.listen(port, ()=> console.log(`Server running at port:${port}`));


